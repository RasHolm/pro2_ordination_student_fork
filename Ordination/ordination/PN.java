package ordination;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;

public class PN extends Ordination {
	private double antalEnheder;
	private ArrayList<LocalDate> givesDenListe = new ArrayList<>();

	public PN(LocalDate startDen, LocalDate slutDen, double antalEnheder) {
		super(startDen, slutDen);
		this.antalEnheder = antalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Returner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		if (givesDen.isAfter(getStartDen().minusDays(1)) && givesDen.isBefore(getSlutDen().plusDays(1))) {
			givesDenListe.add(givesDen);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */

	public int getAntalGangeGivet() {
		return givesDenListe.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	public Laegemiddel getLaegemiddel() {
		return super.getLaegemiddel();
	}

	public double doegnDosis() {
		if (givesDenListe.size() != 0) {

			if (givesDenListe.size() <= 1
					|| (givesDenListe.get(0).equals(givesDenListe.get(givesDenListe.size() - 1)))) {
				return samletDosis();
			} else {
				double antalDage = Period.between(givesDenListe.get(0), givesDenListe.get(givesDenListe.size() - 1))
						.getDays() + 1;
				return samletDosis() / antalDage;
			}
		} else {
			return 0;
		}
	}

	public double samletDosis() {
		return getAntalEnheder() * getAntalGangeGivet();
	}

	@Override
	public String getType() {
		return "Pro Necessitate";
	}

}
