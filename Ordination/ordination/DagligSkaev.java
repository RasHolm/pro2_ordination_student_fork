package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	public DagligSkaev(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);
	}

	private final ArrayList<Dosis> doser = new ArrayList<>();

	/**
	 * Pre: antal >= 0
	 * 
	 * @return dosis
	 */
	public Dosis opretDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		doser.add(dosis);
		return dosis;
	}

	public ArrayList<Dosis> getDoser() {
		return doser;
	}

	/**
	 * Giver det totale antal af samlet dosis over en periode. Pre: Der skal være
	 * oprettet dosis.
	 * 
	 * @return den totale dosis der er givet i den periode ordinationen er gyldig
	 */

	@Override
	public double samletDosis() {
		return antalDage() * doegnDosis();
	}

	/**
	 * Pre: Der skal være oprettet dosis.
	 * 
	 * @return den gennemsnitlige dosis givet pr dag i den periode ordinationen er
	 *         gyldig
	 */
	@Override
	public double doegnDosis() {
		double result = 0;
		for (Dosis d : doser) {
			result += d.getAntal();
		}
		return result;
	}

	/**
	 * Returnerer ordinationstypen som en String
	 * 
	 * @return
	 */

	@Override
	public String getType() {
		return "Daglig skæv";
	}
}
