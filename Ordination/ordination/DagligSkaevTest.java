package ordination;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

public class DagligSkaevTest {

	DagligSkaev testDS = new DagligSkaev(LocalDate.of(2020, 3, 5), LocalDate.of(2020, 3, 15));

	@Test
	public void testConstructor() {
		assertEquals(LocalDate.of(2020, 3, 5), testDS.getStartDen());
		assertEquals(LocalDate.of(2020, 3, 15), testDS.getSlutDen());
	}

	@Test
	public void testOpretDosis() {
		testDS.opretDosis(LocalTime.of(10, 15), 2);
		testDS.opretDosis(LocalTime.of(10, 30), 2);

		assertEquals(2, testDS.getDoser().get(0).getAntal(), 0.01);
		assertEquals(2, testDS.getDoser().get(1).getAntal(), 0.01);

	}

	@Test
	public void testSamletDosis() {
		assertEquals(0.01, testDS.samletDosis(), 44);
	}

	@Test
	public void testDoegnDosis() {
		assertEquals(0.01, testDS.doegnDosis(), 4);
	}
}
