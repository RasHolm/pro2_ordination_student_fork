package ordination;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

public class DagligFastTest {

	DagligFast testDF = new DagligFast(LocalDate.of(2020, 3, 5), LocalDate.of(2020, 3, 15), 2, 1, 0, 1);

	@Test
	public void testConstructor() {
		assertEquals(LocalDate.of(2020, 3, 5), testDF.getStartDen());
		assertEquals(LocalDate.of(2020, 3, 15), testDF.getSlutDen());
		assertEquals(2, testDF.getDoser()[0].getAntal(), 0.01);
		assertEquals(1, testDF.getDoser()[1].getAntal(), 0.01);
		assertEquals(0, testDF.getDoser()[2].getAntal(), 0.01);
		assertEquals(1, testDF.getDoser()[3].getAntal(), 0.01);
	}

	@Test
	public void testSamletDosis() {
		assertEquals(44, testDF.samletDosis(), 0.01);
	}

	@Test
	public void testDoegnDosis() {
		assertEquals(4, testDF.doegnDosis(), 0.01);
	}

}
