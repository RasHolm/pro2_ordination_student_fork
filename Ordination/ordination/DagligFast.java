package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {

	private double morgen;
	private double middag;
	private double aften;
	private double nat;
	private Dosis[] doser;

	public DagligFast(LocalDate startDen, LocalDate slutDen, double morgen, double middag, double aften, double nat) {
		super(startDen, slutDen);
		this.doser = new Dosis[4];
		this.doser[0] = new Dosis(LocalTime.of(6, 0), morgen);
		this.doser[1] = new Dosis(LocalTime.of(12, 0), middag);
		this.doser[2] = new Dosis(LocalTime.of(18, 0), aften);
		this.doser[3] = new Dosis(LocalTime.of(0, 0), nat);
	}

	public Dosis[] getDoser() {
		return doser;
	}

	public void setDosis(Dosis[] doser) {
		this.doser = doser;
	}

	public double getMorgen() {
		return morgen;
	}

	public double getMiddag() {
		return middag;
	}

	public double getAften() {
		return aften;
	}

	public double getNat() {
		return nat;
	}

	/**
	 * Pre: Der skal være oprettet dosis.
	 * 
	 * @return den totale dosis der er givet i den periode ordinationen er gyldig
	 */
	@Override
	public double samletDosis() {
		return antalDage() * doegnDosis();
	}

	/**
	 * Pre: Der skal være oprettet dosis.
	 * 
	 * @return den gennemsnitlige dosis givet pr dag i den periode ordinationen er
	 *         gyldig
	 */
	@Override
	public double doegnDosis() {
		double result = 0;
		for (Dosis d : doser) {
			result += d.getAntal();
		}
		return result;
	}

	/**
	 * Returnerer ordinationstypen som en String
	 * 
	 * @return
	 */
	@Override
	public String getType() {
		return "Daglig fast";
	}
}
