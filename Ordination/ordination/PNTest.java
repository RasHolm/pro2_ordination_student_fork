package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

public class PNTest {

	// arrange
	PN p1 = new PN(LocalDate.of(2020, 3, 5), LocalDate.of(2020, 3, 15), 5);
	PN p2 = new PN(LocalDate.of(2020, 3, 5), LocalDate.of(2020, 3, 15), 5);
	PN p3 = new PN(LocalDate.of(2020, 3, 5), LocalDate.of(2020, 3, 15), 5);
	Laegemiddel lm1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");

	@Test
	public void testPN() {
		// act

		// assert
		assertEquals(LocalDate.of(2020, 3, 5), p1.getStartDen());
		assertEquals(LocalDate.of(2020, 3, 15), p1.getSlutDen());
		assertEquals(5, p1.getAntalEnheder(), 0.001);
	}

	@Test
	public void testSamletDosis() {
		// act
		p1.setLaegemiddel(lm1);
		p1.givDosis(LocalDate.of(2020, 3, 6));
		p1.samletDosis();

		// assert
		assertEquals(5, p1.samletDosis(), 0.001);
	}

	@Test
	public void testDoegnDosis() {
		// act
		p1.setLaegemiddel(lm1);
		p2.setLaegemiddel(lm1);
		p3.setLaegemiddel(lm1);

		p1.givDosis(LocalDate.of(2020, 3, 5));
		System.out.println(p1.getAntalGangeGivet());
		System.out.println(p1.getAntalEnheder());

		p1.doegnDosis();

		p2.givDosis(LocalDate.of(2020, 3, 5));
		p2.givDosis(LocalDate.of(2020, 3, 5));
		p2.doegnDosis();

		p3.givDosis(LocalDate.of(2020, 3, 5));
		p3.givDosis(LocalDate.of(2020, 3, 15));
		p3.doegnDosis();

		// assert
		assertEquals(5, p1.doegnDosis(), 0.001);
		assertEquals(10, p2.doegnDosis(), 0.001);
		assertEquals(0.90909, p3.doegnDosis(), 0.001);
	}

	@Test
	public void testGivDosis() {
		// act & assert
		assertTrue(p1.givDosis(LocalDate.of(2020, 3, 5)));
		assertTrue(p1.givDosis(LocalDate.of(2020, 3, 15)));
		assertTrue(p1.givDosis(LocalDate.of(2020, 3, 10)));
		assertFalse(p1.givDosis(LocalDate.of(2020, 3, 4)));
		assertFalse(p1.givDosis(LocalDate.of(2020, 3, 16)));
		assertFalse(p1.givDosis(LocalDate.of(2020, 3, 20)));
	}

}
