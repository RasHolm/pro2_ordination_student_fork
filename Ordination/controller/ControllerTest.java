package controller;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class ControllerTest {

	// Arrange
	Patient p1 = new Patient("111111-1111", "testman1", 0);
	Patient p2 = new Patient("111111-1111", "testman2", 15);
	Patient p3 = new Patient("111111-1111", "testman3", 25);
	Patient p4 = new Patient("111111-1111", "testman4", 26);
	Patient p5 = new Patient("111111-1111", "testman5", 80);
	Patient p6 = new Patient("111111-1111", "testman6", 120);
	Patient p7 = new Patient("111111-1111", "testman7", 121);
	Patient p8 = new Patient("111111-1111", "testman8", 130);
	Laegemiddel lm1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
	LocalDate startDen = LocalDate.of(2020, 3, 5);
	LocalDate slutDen = LocalDate.of(2020, 3, 15);

	@Test
	public void testOpretPNOrdination() {
		// act
		PN newPN = Controller.getTestController().opretPNOrdination(startDen, slutDen, p5, lm1, 5);

		// assert
		assertEquals(LocalDate.of(2020, 3, 5), newPN.getStartDen());
		assertEquals(LocalDate.of(2020, 3, 15), newPN.getSlutDen());
		assertTrue(p5.getOrdinationer().contains(newPN));
		assertEquals(lm1, newPN.getLaegemiddel());
		assertEquals(5, newPN.getAntalEnheder(), 0.001);

	}

	@Test
	public void testOpretDagligFastOrdination() {
		// act
		DagligFast newDF = Controller.getTestController().opretDagligFastOrdination(startDen, slutDen, p5, lm1, 2, 1, 0,
				1);
		// assert
		assertEquals(LocalDate.of(2020, 3, 5), newDF.getStartDen());
		assertEquals(LocalDate.of(2020, 3, 15), newDF.getSlutDen());
		assertTrue(p5.getOrdinationer().contains(newDF));
		assertEquals(lm1, newDF.getLaegemiddel());
		assertEquals(2, newDF.getDoser()[0].getAntal(), 0.001);
		assertEquals(1, newDF.getDoser()[1].getAntal(), 0.001);
		assertEquals(0, newDF.getDoser()[2].getAntal(), 0.001);
		assertEquals(1, newDF.getDoser()[3].getAntal(), 0.001);
	}

	@Test
	public void testOpretDagligSkaevOrdination() {
		// act
		LocalTime[] klokkeSlet = { LocalTime.of(10, 15), LocalTime.of(10, 30) };
		double[] antalEnheder = { 2, 2 };
		DagligSkaev newDS = Controller.getTestController().opretDagligSkaevOrdination(startDen, slutDen, p5, lm1,
				klokkeSlet, antalEnheder);
		newDS.opretDosis(LocalTime.of(10, 15), 2);
		newDS.opretDosis(LocalTime.of(10, 30), 2);

		// assert
		assertEquals(LocalDate.of(2020, 3, 5), newDS.getStartDen());
		assertEquals(LocalDate.of(2020, 3, 15), newDS.getSlutDen());
		assertTrue(p5.getOrdinationer().contains(newDS));
		assertSame(lm1, newDS.getLaegemiddel());
		assertEquals(2, newDS.getDoser().get(0).getAntal(), 0.001);
		assertEquals(2, newDS.getDoser().get(1).getAntal(), 0.001);
	}

	@Test
	public void testOrdinationPNAnvendt() {
		// arrange
		PN p0 = new PN(startDen, slutDen, 2);

		// act
		Controller.getTestController().ordinationPNAnvendt(p0, LocalDate.of(2020, 3, 10));

		// assert
		assertTrue(p0.givDosis(LocalDate.of(2020, 3, 10)));
	}

	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();

	@Test
	public void testOrdinationPNAnvendtException() {
		exceptionRule.expect(RuntimeException.class);
//		exceptionRule.expectMessage("Error in age or years without damage expected");

		// Arrange
		PN p0 = new PN(startDen, slutDen, 2);

		// Act
		Controller.getTestController().ordinationPNAnvendt(p0, LocalDate.of(2020, 3, 16));

	}

	@Test
	public void testAnbefaletDosisPrDoegn() {
		// act
		double d1 = Controller.getTestController().anbefaletDosisPrDoegn(p1, lm1);
		double d2 = Controller.getTestController().anbefaletDosisPrDoegn(p2, lm1);
		double d3 = Controller.getTestController().anbefaletDosisPrDoegn(p3, lm1);
		double d4 = Controller.getTestController().anbefaletDosisPrDoegn(p4, lm1);
		double d5 = Controller.getTestController().anbefaletDosisPrDoegn(p5, lm1);
		double d6 = Controller.getTestController().anbefaletDosisPrDoegn(p6, lm1);
		double d7 = Controller.getTestController().anbefaletDosisPrDoegn(p7, lm1);
		double d8 = Controller.getTestController().anbefaletDosisPrDoegn(p8, lm1);

		// assert
		assertEquals(0, d1, 0.001);
		assertEquals(1.5, d2, 0.001);
		assertEquals(2.5, d3, 0.001);
		assertEquals(3.9, d4, 0.001);
		assertEquals(12, d5, 0.001);
		assertEquals(18, d6, 0.001);
		assertEquals(19.36, d7, 0.001);
		assertEquals(20.8, d8, 0.001);
	}

}
